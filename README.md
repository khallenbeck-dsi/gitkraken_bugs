# gitkraken_bugs

Gitkraken fails to display the changes for commits where a symlink to a folder is replaced with an actual folder. The error message is "Cannot read property 'file.name' of undefined".

![screenshot.png](screenshot.png)

